magic_compass = {}
local version = "1.4.0-dev"

dofile(minetest.get_modpath("magic_compass") .. "/SETTINGS.lua")

dofile(minetest.get_modpath("magic_compass") .. "/callbacks.lua")
dofile(minetest.get_modpath("magic_compass") .. "/deserializer.lua")
dofile(minetest.get_modpath("magic_compass") .. "/formspec.lua")
dofile(minetest.get_modpath("magic_compass") .. "/items.lua")
dofile(minetest.get_modpath("magic_compass") .. "/player_manager.lua")

minetest.log("action", "[MAGIC COMPASS] Mod initialised, running version " .. version)


if minetest.get_modpath("aes_xp") then
    --register some achievements
    aes_xp.add_achievement("Tower", 50, 40, 5, "You are parkour PRO!", "magiccompass_parkour.png")
end
